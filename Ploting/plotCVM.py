import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

objects = ('Naive Bayes', 'Decision Tree', 'K Neighbours', 'Logistic Regression')
y_pos = np.arange(len(objects))
performance = [0.7981292663051645,0.9990040839669498,0.9987197426236241,0.884617349123161]

plt.bar(y_pos, performance, align='center', alpha=0.5)
plt.xticks(y_pos, objects)
plt.ylabel('Score')
plt.title('Cross Validation Mean')
plt.ylim([0.75,1])

plt.show()